/**
 * Bentuk grid tic-tac-toe
 * |_|_|_| dari kiri ke kanan, R1,R2,R3
 * |_|_|_| dari kiri ke kanan, R4,R5,R6
 * |_|_|_| dari kiri ke kanan, R7,R8,R9
 * Tiap kotak dapat memiliki nilai 0, 1, atau 2
 * 0 = kotak kosong
 * 1 = kotak diisi oleh player 1 (O)
 * 2 = kotak diisi oleh player 2 (X)
 * 
 * LCD: 4x15, disp ON, cursor ON, blink OFF
 * # = kosong
 * State awal init
 * ####|_|_|_|####
 * ####|_|_|_|####
 * ####|_|_|_|####
 * P1=0#######P2=0
 * 
 * LED: 2 LED, 1 untuk tiap player, menunjukan giliran siapa
 * 
 * Button: 1 button, untuk mereset game
 * 
 * Keypad: 1 Keypad, untuk input player, kedua player menggunakan keypad yang
 * sama untuk input
 */



public class TugasAkhir{
    // Menyimpan sudah berapa giliran berjalan
    // register R17
    private static int giliranCounter;

    // Menyimpan berapa banyak ronde yang dimenangkan oleh player1
    // SRAM BLOCK1
    private static int player1Win = 0;

    // Meniympan berapa banyak ronde yang dimenangkan oleh player2
    // SRAM BLOCK1 +1
    private static int player2Win = 0;

    // Sebagai variabel sementara untuk mengolah data
    // register R16
    private static int temp;

    // alamat SRAM untuk menyimpan player1Win dan player2Win
    // .equ BLOCK1=$60
    private static final int BLOCK1=$60;

    public static void main(String[] args){
        initStack();
    }

    public static void initStack(){
        // init Stack
        ldi temp, low(RAMEND)
        out SPL, temp
        ldi temp, high(RAMEND)
        out SPH, temp

        // init LCD
        initZPointer();
    }
    public static void initZPointer(){
        // TODO inisiasi Z Pointer untuk menunjuk pada LCD_MESSAGE
        initXPointer();
    }

    public static void initXPointer(){
        // TODO inisiasi X Pointer untuk menunjuk pada SRAM BLOCK1,
        initLCD();
    }
    public static void initLCD(){
        // TODO inisiasi LCD untuk, liat di atas ^^^
        initLED();
    }
    public static void initLED(){
        // TODO inisiasi LED, liat di atas ^^^
        initButton();
    }
    public static void initButton(){
        // TODO inisiasi Button, liat di atas ^^^
        initKeypad();
    }
    public static void initKeypad(){
        // TODO inisiasi Keypad, liat diatas ^^^
        reset();
    }
    public static void initInterrupt(){
        // TODO inisiasi interrupt dari button RESET
    }

    /** reset(): mereset game ke keadaan semula, giliranCounter=0,
     *  player1/2Win=0, seluruh kotak tic-tac-toe=0
     */
    public static void reset(){
        player1Win = 0;
        player2Win = 0;
        R1,R2,R3,R4,R5,R6,R7,R8,R9 = 0;
        // update LCD untuk mendisplay state init/awal, liat diatas ^^^
        start();
    }

    /** start(): inisiasi sebuah ronde baru */
    public static void start(){
        // cek jika ada player yang sudah menang 3 ronde
        if(player1Win == 3)player1Wins();
        if(player2Win == 3)player2Wins();
        giliranCounter = 0;
        cekGiliranSiapa();
    }

    /** cekGiliranSiapa(): menentukan giliran player1 atau player2,
     *  jika giliranCounter genap maka giliran player1,
     *  jika giliranCounter ganjil maka giliran player2
     */
    public static void cekGiliranSiapa(){
        temp = giliranCounter;
        while (temp > 0) {
            temp -= 2;
        }
        if(temp == 0){
            // update LED untuk menunjukan bahwa sekarang giliran player 1
            giliranPlayer1();
        }else{
            // update LED untuk menunjukan bahwa sekarang giliran player 2
            giliranPlayer2();
        }
    }

    /** giliranPlayer1(): pada fungsi ini ada dua fitur dari AVR yang digunakan,
     *  yaitu time interrupt dan input dari keypad.
     *  Jika dalam waktu delay tertentu player tidak juga memasukan input dari keypad,
     *  maka akan langsung jump ke fungsi apakahPlayer1Menang().
     *  Hint:
     *  fungsi delay_00/delay_01/delay_02 dari lab POK,
     *  slide time interrupt AVR,
     *  AVR keypad
     */
    public static void giliranPlayer1(){
        // TODO
        temp = 0;
        // Fitur time interrupt, inputPlayer berisi nomor grid tic-tac-toe yang
        // ingin diisi
        temp = inputPlayer1;
        // jika player memasukan input, maka hentikan time interrupt(timer) dari countdown,
        // lalu cek tiap grid (R1 - R9),
        // jika grid tersebut 0, maka isi dengan 1,
        // jika tidak, maka langsung skip ke apakahPlayer1Menang()

        // update LCD untuk merepresentasikan keadaan kotak tic-tac-toe sekarang
        apakahPlayer1Menang();
    }

    /** giliranPlayer2(): pada fungsi ini ada dua fitur dari AVR yang digunakan,
     *  yaitu time interrupt dan input dari keypad.
     *  Jika dalam waktu delay tertentu player tidak juga memasukan input dari keypad,
     *  maka akan langsung jump ke fungsi apakahPlayer2Menang().
     *  Hint:
     *  fungsi delay_00/delay_01/delay_02 dari lab POK,
     *  slide time interrupt AVR,
     *  AVR keypad
     */
    public static void giliranPlayer2(){
        // TODO
        temp = 0;
        // Fitur time interrupt, inputPlayer berisi nomor grid tic-tac-toe yang
        // ingin diisi
        temp = inputPlayer2;
        // jika player memasukan input, maka hentikan time interrupt(timer) dari countdown,
        // lalu cek tiap grid (R1 - R9),
        // jika grid tersebut 0, maka isi dengan 2,
        // jika tidak, maka langsung skip ke apakahPlayer2Menang()
        
        // update LCD untuk merepresentasikan keadaan kotak tic-tac-toe sekarang
        apakahPlayer2Menang();
    }

    /** apakahPlayer1Menang(): cek tiap grid(R1 - R9) jika ada yang membentuk 3 baris yang bernilai 1 */
    public static void apakahPlayer1Menang(){
        // TODO: cek tiap grid(R1 - R9),
        // jika ada yang membentuk garis 3 grid bernilai 1 maka,
        player1Win++;
        // update LCD untuk jumlah P1=
        start();
        // jika tidak ada maka
        cekApakahGridPenuh();
    }

    /** apakahPlayer2Menang(): cek tiap grid(R1 - R9) jika ada yang membentuk 3 baris yang bernilai 2 */
    public static void apakahPlayer2Menang(){
        // TODO: cek tiap grid(R1 - R9),
        // jika ada yang membentuk garis 3 grid bernilai 2 maka,
        player2Win++;
        // update LCD untuk jumlah P2=
        start();
        // jika tidak ada maka
        cekApakahGridPenuh();
    }

    /**cekApakahGridPenuh(): cek jika apakah semua grid telah terisi,
     * jika sudah maka, game akan dianggap seri dan lanjut ke ronde baru
     */
    public static void cekApakahGridPenuh(){
        // TODO: cek tiap grid(R1 - R9),
        // jika tidak ada yang bernilai nol/tidak ada kotak yang kosong maka,
        LCD_Update_DRAW();
        start();
        // jika masih ada yang bernilai nol/ada kotak yang kosong maka,
        cekGiliranSiapa();
    }

    public static void player1Wins(){
        // TODO arahkan Z Pointer untuk mengambil pesan PLAYER 1 WINS dan tampilkan ke LCD
    }


    public static void player2Wins(){
        // TODO arahkan Z Pointer untuk mengambil pesan PLAYER 2 WINS dan tampilkan ke LCD
    }

    /** READ_MESSAGE_LCD(): berfungsi untuk meload LCD_MESSAGE dengan Z Pointer */
    public static void READ_MESSAGE_LCD(){
        // TODO load message yang sudah di point oleh Z Pointer
    }
    public static void DISPLAY_MESSAGE_LCD(){
        // TODO display message yang sudah di load dari Z Pointer
    }

    // LCD_MESSAGE: berisi pesan-pesan apa aja yang ingin ditampil ke LCD, gunakan Z Pointer untuk tiap mengupdate sesuai dengan kebutuhan.
    String[] LCD_MESSAGE = {"P1=", "P2=", // mendisplay berapa round yang sudah dimenangkan player1 atau player2
                            "0", "1",
                            "2", "3",
                            "|", "_", // mendisplay bentuk kotak tic-tac-toe
                            "X", "O", // mendisplay kotak yang sudah diisi player
                            "PLAYER 1 WINS", "PLAYER 2 WINS", // mendisplay jika ada player yang menang
                            "DRAW"} // mendisplay jika rondenya seri
}