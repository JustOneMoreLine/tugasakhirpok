.include "m8515def.inc"
.def grid1 = R1
.def grid2 = R2
.def grid3 = R3
.def grid4 = R4
.def grid5 = R5
.def grid6 = R6
.def grid7 = R7
.def grid8 = R8
.def grid9 = R9
.def temp = R16
.def giliranCounter = R17
.def BLOCK1=$60

initStack:
	ldi temp, low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp

initZPointer:
	ldi ZH, high(2*LCD_MESSAGE)
	ldi ZL, low(2*LCD_MESSAGE)
initXPointer:
	; TODO
initLCDmain:
	rcall INIT_LCD
	ser temp
	out DDRA,temp
	out DDRB,temp
initLED:
	ser temp
	out DDRC, temp
initButton:
	;TODO
initKeyPad:
	;TODO

reset:
	;TODO
start:
	;TODO
cekGiliranSiapa:
	;TODO
giliranPlayer1:
	;TODO
giliranPlayer2:
	;TODO
apakahPlayer1Menang:
	;TODO
apakahPlayer2Menang:
	;TODO
cekApakahGridPenuh:
	;TODO
player1Wins:
	;TODO Me
player2Wins:
	;TODO Me
READ_MESSAGE_LCD:
	;TODO Me
DISPLAY_MESSAGE_LCD:
	;TODO Me








INIT_LCD:
	cbi PORTA,1 ; CLR RS
	ldi temp,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,temp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	cbi PORTA,1 ; CLR RS
	ldi temp,$0E ; MOV DATA,0x0C --> disp ON, cursor OFF, blink OFF
	out PORTB,temp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	rcall CLEAR_LCD ; CLEAR LCD
	cbi PORTA,1 ; CLR RS
	ldi temp,$06 ; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out PORTB,temp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret
CLEAR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi temp,$01 ; MOV DATA,0x01
	out PORTB,temp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

LCD_MESSAGE:
.db "X","O","#","TL"
.db "1","2","3","4"
.db "5","6","7","8"
.db "9","10",0,0
